/*
MAX31855/src/MAX31855.h - MAX31855 interface classes
Copyright (C) 2020 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include <stdint.h>

class MAX31855_reading;

class MAX31855 {
private:
  enum class Memory_mask : uint32_t {
    OC_Fault		= 0x00000001,
    SCG_Fault		= 0x00000002,
    SCV_Fault		= 0x00000004,
    _Reserved1		= 0x00000008,
    Internal_Temp	= 0x0000fff0,
    Any_Fault		= 0x00010000,
    _Reserved2		= 0x00020000,
    Thermocouple_Temp	= 0xfffc0000,
  };

  enum class Memory_rshift : uint8_t {
    Internal_Temp	= 4,
    Thermocouple_Temp	= 18,
  };

  uint8_t _cs_pin;
  bool _oc_fault, _scg_fault, _scv_fault, _fault;

public:
  MAX31855(uint8_t cs);

  unsigned long measure(void);

  bool oc_fault(void) const { return _oc_fault; }
  bool scg_fault(void) const { return _scg_fault; }
  bool scv_fault(void) const { return _scv_fault; }
  bool fault(void) const { return _fault; }

  MAX31855_reading* read(void);

}; // class MAX31855

class MAX31855_reading {
private:
  uint16_t _tc, _ij;

  friend class MAX31855;

  MAX31855_reading(uint16_t tc, uint16_t ij);

public:
  float thermocoupleTemperature(void) const;

  float internalTemperature(void) const;

}; // class MAX31855_reading
