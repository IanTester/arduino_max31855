/*
MAX31855/src/MAX31855.cpp - MAX31855 interface classes
Copyright (C) 2020 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "MAX31855.h"

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include <SPI.h>


MAX31855::MAX31855(uint8_t cs) :
  _cs_pin(cs),
  _oc_fault(false),
  _scg_fault(false),
  _scv_fault(false),
  _fault(false)
{
  pinMode(_cs_pin, OUTPUT);
  digitalWrite(_cs_pin, HIGH);
}

unsigned long MAX31855::measure(void) {
  SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));

  _oc_fault = _scg_fault = _scv_fault = _fault = false;
  digitalWrite(_cs_pin, LOW);
  return 100000; // 100 ms
}

MAX31855_reading* MAX31855::read(void) {
  uint32_t data = 0;
  for (uint8_t i = 0; i < 4; i++) {
    data <<= 8;
    data |= SPI.transfer(0x00);
  }

  digitalWrite(_cs_pin, HIGH);
  SPI.endTransaction();

  _oc_fault = data & static_cast<uint32_t>(Memory_mask::OC_Fault);
  _scg_fault = data & static_cast<uint32_t>(Memory_mask::SCG_Fault);
  _scv_fault = data & static_cast<uint32_t>(Memory_mask::SCV_Fault);
  _fault = data & static_cast<uint32_t>(Memory_mask::Any_Fault);

  if (_fault)
    return nullptr;

  return new MAX31855_reading((data & static_cast<uint32_t>(Memory_mask::Thermocouple_Temp)) >> static_cast<uint8>(Memory_rshift::Thermocouple_Temp),
			      (data & static_cast<uint32_t>(Memory_mask::Internal_Temp)) >> static_cast<uint8>(Memory_rshift::Internal_Temp));
}


MAX31855_reading::MAX31855_reading(uint16_t tc, uint16_t ij) :
  _tc(tc),
  _ij(ij)
{
  // sign extension
  if (_tc & 0x2000)
    _tc |= 0xc000;

  if (_ij & 0x800)
    _ij |= 0xf000;
}

float MAX31855_reading::thermocoupleTemperature(void) const {
  return (int16_t)_tc * 0.25;
}

float MAX31855_reading::internalTemperature(void) const {
  return (int16_t)_ij * 0.0625;
}
